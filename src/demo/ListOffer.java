/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;
import java.io.Serializable;
import java.util.List;  
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
  
import javax.xml.bind.annotation.XmlAttribute;  
import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlRootElement; 

/**
 *
 * @author Huong
 */
@XmlRootElement(name = "ListOffer")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class ListOffer implements Serializable {
    private List<Offer> Offers;
    

    public ListOffer() {
        super();
    }
    @XmlElement
    public List<Offer> getOffers() {
        return Offers;
    }

    public void setOffers(List<Offer> Offers) {
        this.Offers = Offers;
    }
    
    
    
}
