/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

import java.io.StringReader;
import java.util.List;
import javax.swing.text.Document;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Huong
 */
public class Demo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        String xml = "<ListOffer>\n"
            + "               <Offer>\n"
            + "                  <Name>RM_ALL</Name>\n"
            + "                  <Id>88888</Id>\n"
            + "                  <Version>1</Version>\n"
            + "                  <ExpDate>10/10/2100 10:10:10 AM</ExpDate>\n"
            + "                  <EffDate>4/7/2020 3:38:32 PM</EffDate>\n"
            + "                  <IdMember/>\n"
            + "                  <ListMember/>\n"
            + "                  <State>1</State>\n"
            + "                  <RecurringDate>2020/04/07 15:46:22</RecurringDate>\n"
            + "                  <UpgradeOrNot>1</UpgradeOrNot>\n"
            + "                  <UpgradeTime>0</UpgradeTime>\n"
            + "                  <DowngradeTime>0</DowngradeTime>\n"
            + "               </Offer>\n"
            + "               <Offer>\n"
            + "                  <Name>4G_S10</Name>\n"
            + "                  <Id>2308</Id>\n"
            + "                  <Version>1</Version>\n"
            + "                  <ExpDate>10/10/2100 10:10:10 AM</ExpDate>\n"
            + "                  <EffDate>6/8/2020 5:40:28 PM</EffDate>\n"
            + "                  <IdMember/>\n"
            + "                  <ListMember/>\n"
            + "                  <State>1</State>\n"
            + "                  <RecurringDate>2020/06/08 17:40:28</RecurringDate>\n"
            + "                  <UpgradeOrNot>0</UpgradeOrNot>\n"
            + "                  <UpgradeTime>0</UpgradeTime>\n"
            + "                  <DowngradeTime>0</DowngradeTime>\n"
            + "               </Offer>\n"
            + "</ListOffer>";

        JAXBContext jaxbContext;
        try {
            StringReader str = new StringReader(xml);
            jaxbContext = JAXBContext.newInstance(ListOffer.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            ListOffer offer = (ListOffer) jaxbUnmarshaller.unmarshal(str);
            
            
            
            List<Offer> list=offer.getOffers();
            for(Offer ans:list){
                System.out.println(ans.getName());
            }
            
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

}
